// eslint-disable-next-line @typescript-eslint/no-var-requires
const purgecss = require("@fullhuman/postcss-purgecss")({
	// Specify the paths to all of the template files in your project
	content: [
		"./index.html",
		"./src/**/*.vue",
		"./src/**/*.ts",
		// etc.
	],
	// Include any special characters you're using in this regular expression
	defaultExtractor: (content) => content.match(/[^<>"'`\s]*[^<>"'`\s:]/g) || [],
	// defaultExtractor: (content) => content.match(/[\w-/.:]+(?<!:)/g) || [],
	// Whitelist transition, transition-group and router-link class
	whitelistPatterns: [
		/-(leave|enter|appear)(|-(to|from|active))$/,
		/^(?!(|.*?:)cursor-move).+-move$/,
		/^router-link(|-exact)-active$/,
	],
});

module.exports = {
	plugins: [
		require("tailwindcss"),
		...(process.env.NODE_ENV == "production" ? [purgecss] : []),
	],
};
