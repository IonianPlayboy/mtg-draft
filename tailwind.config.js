/* eslint-disable vue/sort-keys */

// eslint-disable-next-line @typescript-eslint/no-var-requires
const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
    purge: false,
    theme: {
        extend: {
            // fontFamily: {
            // 	display: ["Oswald", ...defaultTheme.fontFamily.sans],
            // 	body: [
            // 		"Roboto",
            // 		...defaultTheme.fontFamily.sans.filter((el) => el !== "Roboto"),
            // 	],
            // },
            colors: {
                textMain: "#F4F4F4",
                textShadowLight: "#F4F4F480",
                textSubdued: "#CFCFCF",
                backgroundMain: "#121212",
                headerTop: "#383A3C",
                headerBackground: "#232527",
                buttonTop: "#444749",
                buttonBackground: "#303336",
                cardBackground: "#232527",
                eventBackground: "#433D3D",
                secondaryBackground: "#383A3C",
                primaryCustom: "#924576",
                secondaryCustom: "#45926D",
                tertiaryCustom: "#457B92",

                magicWhite: "#6F765A",
                magicBlue: "#3D4778",
                magicBlack: "#413F44",
                magicRed: "#7B2333",
                magicGreen: "#235C15",
                magicColorless: "#6A6667",
                magicGold: "#645430",
            },
            inset: {
                "2px": "2px",
                100: "100%",
            },
        },
    },
    variants: {},
    plugins: [],
    future: {
        removeDeprecatedGapUtilities: true,
        purgeLayersByDefault: true,
    },
};
